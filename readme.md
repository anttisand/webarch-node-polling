# Node.js asynchronous communication polling sample

Polling the server for new data is a really inefficient way of asynchronous communication as we need to keep calling the server multiple times and only very seldom get any new data back.

Further, there is a delay. We can make the polling happen more frequently by decreasing the interval between polls (thus increasing the number of unneccessary calls to the server), but we can never have true real-time transactions.

## Installation & running instructions

```npm install``` to pull in the required packages.

```node index.js``` will start the server listening on port 3000.

Open a browser and navigate to ```localhost:3000```. Note, that you can open multiple browser windows/tabs to simulate multiple users discussing with each other.

### Notes

As the backend does not use a database, but rather has the messages in memory, everything will be wiped clean each time you stop the server. However, integrating, for example, mongoDB as a persistent data storage is quite trivial.