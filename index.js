const express = require('express'); //This simplifies handling of requests and responses
const bodyParser = require('body-parser'); //Bodyparser extracts the entire body portion of an incoming request
const cors = require('cors');
const app = express(); //Create a new express server

app.use(express.static(__dirname)); //Serve static files
app.use(bodyParser.json()); //Set bodyparser to use JSON
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors());

//Set an initial message to be shown to all clients upon first poll
let messages = [
    {
        name: "PollingSystem",
        message: "Welcome to polling sample"
    }
];

//This is an API route meant to be accessed programmatically, but nothing stops users from accessing this endpoint directly
//Sends all messages as a response to requests
app.get('/messages', (request, response) => {
    response.send(messages);
});

//This is an API route meant to be accessed programmatically, but users may also post here with Postman or cUrl, for example.
//Be aware, that this might be a security issue depending on what kind of an application you are creating.
//So, authorization could be added here
app.post('/messages', (request, response) => {
    //Assume, that we get valid data. However, there's no guarantees, that the data is valid.
    let message = request.body;

    //Add the new message to the messages array in memory
    messages.push(message);

    //Simply responds with a status code to inform the client, that everything went OK. 
    response.sendStatus(200); //HTTP 200 OK
});

const server = app.listen(3000, () => {
    console.log(`Server is running on port ${server.address().port}`);
});